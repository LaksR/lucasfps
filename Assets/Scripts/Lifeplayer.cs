﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Lifeplayer : MonoBehaviour {
public int healthplayer = 100;
public GameObject iniImage;

public GameObject HP_100;
public GameObject HP_75;  
public GameObject HP_65;
public GameObject HP_45;    
public GameObject HP_15;
public GameObject HP_0;
 
void Update()
    {
  if(healthplayer == 100)
     {
      HP_100.SetActive(true);
      HP_75.SetActive(false);
      HP_65.SetActive(false);
      HP_45.SetActive(false);
      HP_15.SetActive(false);
      HP_0.SetActive(false);
     }
     if(healthplayer == 85)
     {
      HP_100.SetActive(false);
      HP_75.SetActive(true);
      HP_65.SetActive(false);
      HP_45.SetActive(false);
      HP_15.SetActive(false);
      HP_0.SetActive(false);
     }
     if(healthplayer == 70)
     {
      HP_100.SetActive(false);
      HP_75.SetActive(false);
      HP_65.SetActive(true);
      HP_45.SetActive(false);
      HP_15.SetActive(false);
      HP_0.SetActive(false);
     }
	 if(healthplayer == 55)
     {
      HP_100.SetActive(false);
      HP_75.SetActive(false);
      HP_65.SetActive(false);
      HP_45.SetActive(true);
      HP_15.SetActive(false);
      HP_0.SetActive(false);
     }
	 if(healthplayer == 40)
     {
      HP_100.SetActive(false);
      HP_75.SetActive(false);
      HP_65.SetActive(false);
      HP_45.SetActive(false);
      HP_15.SetActive(true);
      HP_0.SetActive(false);
     }
	 if(healthplayer == 25)
     {
      HP_100.SetActive(false);
      HP_75.SetActive(false);
      HP_65.SetActive(false);
      HP_45.SetActive(false);
      HP_15.SetActive(false);
      HP_0.SetActive(true);
     }
     if (healthplayer == 10){
       SceneManager.LoadScene("MainOver");
     }
   }
   
    } 
