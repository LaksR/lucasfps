﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TIMEController : MonoBehaviour {
	float timer;
	public Text txt;

	public Image timeBar;

	private float horas, minutos, segundos;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		segundos += Time.deltaTime;
		if(segundos >= 59f){

			minutos ++;
			timeBar.fillAmount += 0.033f;
			segundos = 0f;
			if(minutos >= 59f){

				horas ++;
				minutos = 0f;
			}
		}


		txt.text = horas.ToString("00:")+ minutos.ToString("00:")+ segundos.ToString("00");

	}
		
}
