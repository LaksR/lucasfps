﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuertaCasaController : MonoBehaviour {
	public GameObject eopendoor;
	private Animator anim;
	public AudioSource opendoor;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider car){
		eopendoor.SetActive(true);
		if(Input.GetKeyDown(KeyCode.E)){
			eopendoor.SetActive(false);
			anim.SetBool("open",true); //Animacion
			opendoor.enabled = true;
			this.GetComponent<MeshCollider>().enabled = false;

		}
	}

	void OnTriggerExit(Collider car){
		eopendoor.SetActive(false);
		opendoor.Stop();	
	}

}
