﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PauseController : MonoBehaviour {
	public GameObject can;
	public GameObject SistemaLogrosONoff;
	public GameObject MusicaPanel;
	public GameObject OptionPanel;
	public GameObject AyudaPanel;

	public GameObject BOTTONMAP;

	public GameObject[] mb;
	public GameObject M;

	public Animator Anim1;
	public Animator Anim2;
	public Animator Anim3;
	public Animator Anim4;
	public Animator Anim5;

	bool log2 = true;
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape)){
			if(Time.timeScale == 0f){
				Time.timeScale = 1f;
				can.SetActive(false);
			}else if (Time.timeScale == 1f){
				can.SetActive(true);
				Time.timeScale = 0f;
			}
		}

		if (Input.GetKeyDown(KeyCode.M)){
			if(M.active == false){
				M.SetActive(true);
			}else if (M.active == true){
				M.SetActive(false);
			}
		}
	}

	public void Canactive(){
		if(Time.timeScale == 0f){
			Time.timeScale = 1f;
			can.SetActive(false);
		}else if (Time.timeScale == 1f){
			can.SetActive(true);
			Time.timeScale = 0f;
		}
	}

	public void Cancelar(){
		Time.timeScale = 1f;
		can.SetActive(false);
	}

	public void SalirDelJuego(){
		Application.Quit();
	}

	public void Musica(){
		MusicaPanel.SetActive(true);
	}

	public void MusicaOFF(){
		MusicaPanel.SetActive(false);
	}

	public void Opciones(){
		OptionPanel.SetActive(true);
	}

	public void OpcionesOFF(){
		OptionPanel.SetActive(false);
	}

	public void Sistemalogros(){
		SistemaLogrosONoff.SetActive(true);


	}

	public void SistemaLogrosOFF(){
		SistemaLogrosONoff.SetActive(false);
	}

	public void Ayuda(){
		AyudaPanel.SetActive(true);
	}

	public void AyudaOFF(){
		AyudaPanel.SetActive(false);
	}

	public void MapXOFF(){
		for (int i = 0 ; i <= mb.Length -1 ; i++){
			mb[i].SetActive(false);
		}
		BOTTONMAP.SetActive(true);
	}

	public void MapXON(){
		for (int i = 0 ; i <= mb.Length -1 ; i++){
			mb[i].SetActive(true);
		}
		BOTTONMAP.SetActive(false);
	}

	public void MOFF(){
		M.SetActive(false);
	}

	public void MON(){
		M.SetActive(true);
	}
}
