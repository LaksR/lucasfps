﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGeneratortwo : MonoBehaviour {
/* 
	public GameObject enemy;
	public Transform[] points;
	public float timeToSpawn;
	public Transform pointtospawn;
	IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);
			
			GameObject ienemy = Instantiate (enemy,pointtospawn);
			EnemyBehaviour cb = ienemy.GetComponent<EnemyBehaviour> ();
			cb.pathNodes = points;


		}
	}*/

    public int enemyCount = 0;
    public int maxEnemyCount = 12;
    public float timeBetweenSpawns = 2.0f;
    public GameObject enemy;
	public Transform[] points;
	public Transform pointtospawn;
	public EnemyBehaviour eb;
	bool spanw_On = true;
    void Start(){

		spanw_On = true;
		InvokeRepeating("Spawn", 0, timeBetweenSpawns);
		
        
		
    }
     

	
    void Spawn(){

		if(spanw_On){
       
         //Your instantiate stuff here
    	 GameObject ienemy = Instantiate (enemy,pointtospawn);
			EnemyBehaviourtwo cb2 = ienemy.GetComponent<EnemyBehaviourtwo> ();
			cb2.pathNodes = points;
			cb2.eg2 = this;
			enemyCount++;
	  	}


         if(enemyCount>=maxEnemyCount){
			  spanw_On = false;			  
         }else{
			 spanw_On = true;
		 }

		
		 
    }

}


