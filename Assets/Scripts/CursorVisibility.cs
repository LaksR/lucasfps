﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorVisibility : MonoBehaviour {

	CursorLockMode wantedMode;

void OnLevelWasLoaded(int MainOver)
{
    if (FindObjectOfType<FPSInputManager>() != null)
    {
        Cursor.visible = false;
		Cursor.lockState = wantedMode = CursorLockMode.None;
    }
    else
    {
        Cursor.visible = true;
		Cursor.lockState = wantedMode = CursorLockMode.None;
    }
}
}
